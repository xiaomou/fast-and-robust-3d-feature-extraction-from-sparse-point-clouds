#ifndef PROJECT_FEATURE_H
#define PROJECT_FEATURE_H

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <opencv2/opencv.hpp>

struct FeatureParams{
    float max_vert,min_vert;
    int rows,cols;
    //flat removal
    float flat_removal_dis_thre;
    int flat_removal_num_thre;
    //normal compute
    float normal_compute_range_thre;
    //cluster
    float cluster_angle_thre;
    float cluster_dis_thre;
    int cluster_pts_thre;
    //line or plane
    float line_thre,line_error_thre;
    float plane_thre,plane_error_thre;
    FeatureParams(){
        max_vert = 5.;
        min_vert = -24.;
        rows = 64;
        cols = 1600;
        flat_removal_dis_thre = 0.1;
        flat_removal_num_thre = 5;
        normal_compute_range_thre = 0.1;
        cluster_angle_thre = 5.;
        cluster_dis_thre = .5;
        cluster_pts_thre = 30;
        line_thre = 0.5;
        line_error_thre = 0.1;
        plane_thre = 0.5;
        plane_error_thre = 0.1;
    }
};
class Feature{
public:
    Feature(const FeatureParams& config);

    void generateFeature(const pcl::PointCloud<pcl::PointXYZI>::ConstPtr in_cloud_ptr);

    void getLineRes(std::vector<std::vector<int> >& idx,std::vector<Eigen::Vector3f>& dir,std::vector<Eigen::Vector3f>& centers);
    void getPlaneRes(std::vector<std::vector<int> >& idx,std::vector<Eigen::Vector3f>& norms,std::vector<Eigen::Vector3f>& centers);

    ~Feature(){}

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr valid_feature_ptr_;
    pcl::PointCloud<pcl::Normal>::Ptr valid_normal_ptr_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr valid_ptr_;
    std::vector<pcl::PointCloud<pcl::PointXYZI>::Ptr> cluster_;
private:
    struct ResData{
        std::vector<std::vector<int> > idx;
        std::vector<Eigen::Vector3f> vec;
        std::vector<Eigen::Vector3f> center;
    };
    ResData line_data_,plane_data_;
    float calcuPointToLine(const Eigen::Vector3f& dir,const Eigen::Vector3f& pt_in_line,const pcl::PointXYZI& pt);
    float calcuPointToPlane(const Eigen::Vector4f& plane_param,const pcl::PointXYZI& pt);
    float vert_res_,hori_res_;
    FeatureParams config_;
};

#endif //PROJECT_FEATURE_H
