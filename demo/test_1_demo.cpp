#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>
#include <opencv2/opencv.hpp>
#include <pcl/visualization/pcl_visualizer.h>
#include "feature.h"

int main(int argc, char **argv) {
    std::string pcd_str = "/media/mjj/95fdb55d-55e7-4908-9f81-cd0cc49d8b33/test/data/pcd/000000.pcd";

    pcl::PointCloud<pcl::PointXYZI>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>);

    if (pcl::io::loadPCDFile(pcd_str.c_str(),*in_cloud_ptr) == -1){
        std::cerr<<"Load pcd file failed!"<<std::endl;
        exit(-1);
    }

    std::cout<<"nums: "<<in_cloud_ptr->size()<<std::endl;

    FeatureParams config;
    Feature feature(config);

    feature.generateFeature(in_cloud_ptr);
    std::vector<std::vector<int> > line_idx;
    std::vector<Eigen::Vector3f> line_factor;
    std::vector<Eigen::Vector3f> line_pt;

    std::vector<std::vector<int> > plane_idx;
    std::vector<Eigen::Vector3f> plane_factor;
    std::vector<Eigen::Vector3f> plane_pt;

    feature.getLineRes(line_idx,line_factor,line_pt);
    feature.getPlaneRes(plane_idx,plane_factor,plane_pt);

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr color_cloud_ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
    for (int k = 0; k < static_cast<int>(line_idx.size()); ++k) {
        for (int i = 0; i < line_idx[k].size(); ++i) {
            pcl::PointXYZRGB tmp_rgb;
            const auto& pt = in_cloud_ptr->points[line_idx[k][i]];
            tmp_rgb.x = pt.x;
            tmp_rgb.y = pt.y;
            tmp_rgb.z = pt.z;
            tmp_rgb.r = 255;
            color_cloud_ptr->push_back(tmp_rgb);
        }
    }
    for (int k = 0; k < static_cast<int>(plane_idx.size()); ++k) {
        for (int i = 0; i < plane_idx[k].size(); ++i) {
            pcl::PointXYZRGB tmp_rgb;
            const auto& pt = in_cloud_ptr->points[plane_idx[k][i]];
            tmp_rgb.x = pt.x;
            tmp_rgb.y = pt.y;
            tmp_rgb.z = pt.z;
            tmp_rgb.g = 255;
            color_cloud_ptr->push_back(tmp_rgb);
        }
    }
    pcl::visualization::CloudViewer viewer ("Viewer");
    viewer.showCloud (color_cloud_ptr);
    while (!viewer.wasStopped ())
    {
    }

    return 0;
}