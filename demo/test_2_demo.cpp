#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/centroid.h>
#include <pcl/features/feature.h>
#include <opencv2/opencv.hpp>
#include <Eigen/Eigenvalues>

int main(int argc, char **argv) {

    int size = 100;
    srand((unsigned)time(NULL));
    pcl::PointCloud<pcl::PointXYZI>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>);

    in_cloud_ptr->resize(size);
    for (int j = 0; j < size; ++j) {
        pcl::PointXYZI pt;
        pt.x = static_cast<float>(rand() % 10000) / 10000. * 100;
        pt.y = static_cast<float>(rand() % 10000) / 10000. * 100;
        pt.z = static_cast<float>(rand() % 10000) / 10000. * 100;
        in_cloud_ptr->points[j] = pt;
    }

    Eigen::Matrix3f covariance_matrix;
    Eigen::Vector4f xyz_centroid;
    pcl::computeMeanAndCovarianceMatrix(*in_cloud_ptr, covariance_matrix, xyz_centroid);
    float nx,ny,nz,cur;
    pcl::solvePlaneParameters(covariance_matrix,nx,ny,nz,cur);

    std::cout<<"nx: "<<nx<<" ny: "<<ny<<" nz:"<<nz<<" cur: "<<cur<<std::endl;

//    std::cout<<"matrix: "<<covariance_matrix<<std::endl;

    Eigen::EigenSolver<Eigen::Matrix3f> es(covariance_matrix);
    Eigen::Matrix3f eigenvalues = es.pseudoEigenvalueMatrix();
    Eigen::Matrix3f eigenvectors = es.pseudoEigenvectors();

//    std::cout<<"eigen values: "<<es.eigenvalues()<<std::endl;
    std::cout<<"eigen values: "<<eigenvalues<<std::endl;
//    std::cout<<"eigen vectors: "<<es.eigenvectors()<<std::endl;
    std::cout<<"eigen vectors: "<<eigenvectors<<std::endl;
    std::cout<<"eigen vectors: "<<eigenvectors.col(1).cross(eigenvectors.col(2)) <<std::endl;

    return 0;
}