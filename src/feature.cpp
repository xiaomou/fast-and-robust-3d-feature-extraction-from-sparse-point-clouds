#include "feature.h"
#include <pcl/features/normal_3d.h>
#include <stack>
#include <pcl/visualization/cloud_viewer.h>

Feature::Feature(const FeatureParams& config){
    config_ = config;
    config_.max_vert = (config_.max_vert) / 180. * M_PI;
    config_.min_vert = (config_.min_vert) / 180. * M_PI;
    config_.cluster_angle_thre = config_.cluster_angle_thre / 180. * M_PI;
    vert_res_ = (config_.max_vert - config_.min_vert) / static_cast<float>(config_.rows);
    hori_res_ = (2. * M_PI) / static_cast<float>(config_.cols);
}

void Feature::generateFeature(const pcl::PointCloud<pcl::PointXYZI>::ConstPtr in_cloud_ptr){

    clock_t start = clock();
    //flag: invalid:0,valid:1,feature:2
    //step 1:generate polar range map
    //生成极坐标深度图，range_mat保存深度，idx_mat表示对应点的索引，flag_mat标示label
    cv::Mat range_mat = cv::Mat::zeros(config_.rows,config_.cols,CV_32F);
    cv::Mat idx_mat = cv::Mat::zeros(config_.rows,config_.cols,CV_32S);
    cv::Mat flag_mat = cv::Mat::zeros(config_.rows,config_.cols,CV_32S);
    for (size_t k = 0; k < in_cloud_ptr->size(); ++k) {
        const auto& pt = in_cloud_ptr->points[k];
        if (std::isnan(pt.x) || std::isnan(pt.y) || std::isnan(pt.z)){
            continue;
        }
        float range = std::sqrt(pt.x * pt.x + pt.y * pt.y);
        float hori = std::atan2(pt.y,pt.x);
        float vert = std::atan2(pt.z, range);
        if (hori < 0){
            hori += (2 * M_PI);
        }
        int row = (vert - config_.min_vert) / vert_res_;
        int col = (hori) / hori_res_;

        if (row < 0 || row >= config_.rows || col < 0 || col >= config_.cols){
            continue;
        }
        if (flag_mat.at<int>(row,col) <= 0){
            flag_mat.at<int>(row,col) = 1;
            range_mat.at<float>(row,col) = range;
            idx_mat.at<int>(row,col) = k;
        }else{
            if (range_mat.at<float>(row,col) > range){
                range_mat.at<float>(row,col) = range;
                idx_mat.at<int>(row,col) = k;
            }
        }
    }

    //step2: flat region removal
    //移除平坦的点，论文中是通过极坐标从近到远延伸，如果是平坦的点，则点的高度会差不多，而深度会逐渐变大，而非平坦的点则相反，非平坦点就是论文要找出来的特征点
    //在这里增加一个条件，用以增加特征点的鲁棒性，特征点在极坐标系下是连续的。如果从近到远的时候，点的深度突然变大，则说明特征点从这个地方就断开了。
    for (int col = 0; col < config_.cols; ++col) {
        for (int row = 0; row < config_.rows - config_.flat_removal_num_thre; ++row) {
            if (flag_mat.at<int>(row,col) != 1){
                continue;
            }
            std::vector<int> flag(config_.rows,-1);
            int cc = 0;
            flag[cc] = row;
            cc++;
            float range_1 = range_mat.at<float>(row,col);
            for (int row_in = row + 1; row_in < config_.rows; ++row_in) {
                if (flag_mat.at<int>(row_in,col) != 1){
                    continue;
                }
                float range_2 = range_mat.at<float>(row_in,col);
                if (std::abs(range_1 - range_2) < config_.flat_removal_dis_thre){
                    flag[cc] = row_in;
                    cc++;
                }else{
                    //这个地方用来增加特征点在极坐标系下的连续性
                    break;
                }
            }
            if (cc > config_.flat_removal_num_thre){
                for (int i = 0; i < cc; ++i) {
                    flag_mat.at<int>(flag[i],col) = 2;
                }
                row = flag[cc - 1];
            }
        }
    }
//-----------------------test code: test valid_features---------------//
//    valid_feature_ptr_.reset(new pcl::PointCloud<pcl::PointXYZRGB>);
//    for (int row = 0; row < config_.rows; ++row) {
//        for (int col = 0; col < config_.cols; ++col) {
//            if (flag_mat.at<int>(row,col) == 0){
//                continue;
//            }
//            int idx = idx_mat.at<int>(row,col);
//            const auto&pt = in_cloud_ptr->points[idx];
//            pcl::PointXYZRGB tmp_rgb;
//            tmp_rgb.x = pt.x;
//            tmp_rgb.y = pt.y;
//            tmp_rgb.z = pt.z;
//            if (flag_mat.at<int>(row,col) == 2){
//                int idx = idx_mat.at<int>(row,col);
//                tmp_rgb.g = 255;
//            }else{
//                tmp_rgb.r = 255;
//            }
//            valid_feature_ptr_->push_back(tmp_rgb);
//        }
//    }
//    pcl::visualization::CloudViewer viewer ("Viewer");
//    viewer.showCloud (valid_feature_ptr_);
//    while (!viewer.wasStopped ())
//    {
//    }

    std::vector<Eigen::Vector3f> nornal_vec(config_.rows * config_.cols);
    //step 3 surface normal computation
    //计算每个点的法向量
    int max_pts = 100;
    int internal_pts = 25;
    for (int row = 0; row < config_.rows; ++row) {
        for (int col = 0; col < config_.cols; ++col) {
            if (flag_mat.at<int>(row,col) != 2){
                continue;
            }
            float range = range_mat.at<float>(row,col);

            std::vector<int> idx_vec(max_pts, -1);
            int cc = 0;
            //find left surface_pts
            int left_pt = col;
            int left_pt_cc = 0;
            while (left_pt_cc < internal_pts){
                left_pt--;
                if (left_pt < 0){
                    left_pt = config_.cols - 1;
                }
                if (left_pt == col){
                    break;
                }
                if (flag_mat.at<int>(row,left_pt) <= 0){
                    continue;
                }
                float tmp_range = range_mat.at<float>(row,left_pt);
                if (std::abs(tmp_range - range) < config_.normal_compute_range_thre){
                    idx_vec[cc] = idx_mat.at<int>(row,left_pt);
                    cc++;
                    left_pt_cc++;
                }else{
                    break;
                }
            }

            //find right surface_pts
            int right_pt = col;
            int right_pt_cc = 0;
            while (right_pt_cc < internal_pts){
                right_pt++;
                if (right_pt >= config_.cols){
                    right_pt = 0;
                }
                if (right_pt == col){
                    break;
                }
                if (flag_mat.at<int>(row,right_pt) <= 0){
                    continue;
                }
                float tmp_range = range_mat.at<float>(row,right_pt);
                if (std::abs(tmp_range - range) < config_.normal_compute_range_thre){
                    idx_vec[cc] = idx_mat.at<int>(row,right_pt);
                    cc++;
                    right_pt_cc++;
                }else{
                    break;
                }
            }

            //find up surface_pts
            int up_pt = row;
            int up_pt_cc = 0;
            while (up_pt_cc < internal_pts){
                up_pt++;
                if (up_pt >= config_.rows){
                    break;
                }
                if (flag_mat.at<int>(up_pt,col) <= 0){
                    continue;
                }
                float tmp_range = range_mat.at<float>(up_pt,col);
                if (std::abs(tmp_range - range) < config_.normal_compute_range_thre){
                    idx_vec[cc] = idx_mat.at<int>(up_pt,col);
                    cc++;
                    up_pt_cc++;
                }else{
                    break;
                }
            }

            //find down surface_pts
            int down_pt = row;
            int down_pt_cc = 0;
            while (down_pt_cc < internal_pts){
                down_pt--;
                if (down_pt < 0){
                    break;
                }
                if (flag_mat.at<int>(down_pt,col) <= 0){
                    continue;
                }
                float tmp_range = range_mat.at<float>(down_pt,col);
                if (std::abs(tmp_range - range) < config_.normal_compute_range_thre){
                    idx_vec[cc] = idx_mat.at<int>(down_pt,col);
                    cc++;
                    down_pt_cc++;
                }else{
                    break;
                }
            }

//            for (int y = row - 5; y <= row + 5; ++y) {
//                if (y < 0 || y >= config_.rows){
//                    continue;
//                }
//                for (int x = col - 5; x <=  col + 5; ++x) {
//                    if (x < 0 || x >= config_.cols){
//                        continue;
//                    }
//                    float tmp_range = range_mat.at<float>(y,x);
//                    if (std::abs(tmp_range - range) < config_.normal_compute_range_thre){
//                        idx_vec[cc] = idx_mat.at<int>(y,x);
//                        cc++;
//                    }
//                }
//            }
            if (cc < 4){
                flag_mat.at<int>(row,col) = 1;
                continue;
            }
            idx_vec.resize(cc);
            Eigen::Vector4f plane_parameters;
            float cur;
            pcl::computePointNormal(*in_cloud_ptr,idx_vec,plane_parameters,cur);
            int mat_idx = row * config_.cols + col;
            if (plane_parameters(0) < 0.){
                nornal_vec[mat_idx](0) = -plane_parameters(0);
                nornal_vec[mat_idx](1) = -plane_parameters(1);
                nornal_vec[mat_idx](2) = -plane_parameters(2);
            }else{
                nornal_vec[mat_idx](0) = plane_parameters(0);
                nornal_vec[mat_idx](1) = plane_parameters(1);
                nornal_vec[mat_idx](2) = plane_parameters(2);
            }
        }
    }

//-------------------------test code: test normal pts------------------------
//    valid_normal_ptr_.reset(new pcl::PointCloud<pcl::Normal>);
//    valid_ptr_.reset(new pcl::PointCloud<pcl::PointXYZI>);
//    for (int row = 0; row < config_.rows; ++row) {
//        for (int col = 0; col < config_.cols; ++col) {
//            if (flag_mat.at<int>(row,col) != 2){
//                continue;
//            }
//            pcl::Normal tmp_nornal;
//            tmp_nornal.normal_x = nornal_vec[row * config_.cols + col](0);
//            tmp_nornal.normal_y = nornal_vec[row * config_.cols + col](1);
//            tmp_nornal.normal_z = nornal_vec[row * config_.cols + col](2);
//            int idx = idx_mat.at<int>(row,col);
//            valid_ptr_->push_back(in_cloud_ptr->points[idx]);
//            valid_normal_ptr_->push_back(tmp_nornal);
//        }
//    }
//    pcl::visualization::PCLVisualizer viewer("PCL viewer");
//    viewer.setBackgroundColor(0.0, 0.0, 0.0);
//    viewer.addPointCloudNormals<pcl::PointXYZI, pcl::Normal>(valid_ptr_, valid_normal_ptr_,1,0.5);
//
//    while(!viewer.wasStopped())
//    {
//        viewer.spinOnce();
//    }

    //step4 cluster
    int total_obj_num = 0;
    cv::Mat label_mat = cv::Mat::zeros(config_.rows,config_.cols,CV_32S);
    for (int row = 0; row < config_.rows; ++row) {
        for (int col = 0; col < config_.cols; ++col) {
            if (flag_mat.at<int>(row,col) != 2){
                continue;
            }
            if (label_mat.at<int>(row,col) != 0){
                continue;
            }
            std::stack<cv::Point2i> neighborPixels;     //存储生长种子点的栈容器
            neighborPixels.push(cv::Point2i(col, row)); //以当前栅格点初始化
            total_obj_num++;
            label_mat.at<int>(row, col) = total_obj_num; //初始化label

            while (!neighborPixels.empty()) {
                cv::Point2i cur_index = neighborPixels.top();
                neighborPixels.pop();
                Eigen::Vector3f cur_nornal = nornal_vec[cur_index.y * config_.cols + cur_index.x];
                float cur_norm = cur_nornal.norm();
                pcl::PointXYZI cur_pt = in_cloud_ptr->points[idx_mat.at<int>(cur_index.y,cur_index.x)];

                for (int j = cur_index.y - 3; j <= cur_index.y + 3; ++j) {
                    if (j >= config_.rows || j < 0)
                        continue;
                    for (int i = cur_index.x - 3; i <= cur_index.x + 3; ++i) {
                        if (i >= config_.cols || i < 0)
                            continue;
                        if (flag_mat.at<int>(j, i) != 2)
                            continue;
                        if (label_mat.at<int>(j, i) != 0)
                            continue; //跳过已被标记格
                        Eigen::Vector3f tmp_normal = nornal_vec[j * config_.cols + i];
                        float tmp_norm = tmp_normal.norm();
                        pcl::PointXYZI tmp_pt = in_cloud_ptr->points[idx_mat.at<int>(j,i)];
                        float tmp_dis = std::sqrt((cur_pt.x - tmp_pt.x) * (cur_pt.x - tmp_pt.x) + (cur_pt.y - tmp_pt.y) * (cur_pt.y - tmp_pt.y));
                        if (tmp_dis > config_.cluster_dis_thre){
                            continue;
                        }
                        float theta = std::acos(cur_nornal.dot(tmp_normal) / cur_norm / tmp_norm);
                        if (theta > M_PI / 2.){
                            theta = M_PI - theta;
                        }
                        if (theta > config_.cluster_angle_thre){
                            continue;
                        }
                        label_mat.at<int>(j, i) = total_obj_num;
                        neighborPixels.push(cv::Point2i(i, j));
                    }
                }
            }
        }
    }

    std::vector<std::vector<int> > cluster_idx_vec(total_obj_num);
    for (int row = 0; row < config_.rows; ++row) {
        for (int col = 0; col < config_.cols; ++col) {
            int label = label_mat.at<int>(row,col) - 1;
            if (label < 0 || label >= total_obj_num){
                continue;
            }
            cluster_idx_vec[label].push_back(idx_mat.at<int>(row,col));
        }
    }

    std::cout<<"cost: "<<(clock() - start) * 0.001<<" ms."<<std::endl;

    //get res
    line_data_.idx.clear();
    line_data_.center.clear();
    line_data_.vec.clear();
    plane_data_.idx.clear();
    plane_data_.center.clear();
    plane_data_.vec.clear();
    for (int k = 0; k < total_obj_num; ++k) {
        if (cluster_idx_vec[k].size() < config_.cluster_pts_thre){
            continue;
        }

//------------------test code: test single cluster---------------------
//        pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>);
//        for (int j = 0; j < cluster_idx_vec[k].size(); ++j) {
//            tmp_cloud_ptr->push_back(in_cloud_ptr->points[cluster_idx_vec[k][j]]);
//        }
//        pcl::visualization::CloudViewer viewer ("Viewer");
//        viewer.showCloud (tmp_cloud_ptr);
//        while (!viewer.wasStopped ())
//        {
//        }

        Eigen::Matrix3f covariance_matrix;
        Eigen::Vector4f xyz_centroid;
        pcl::computeMeanAndCovarianceMatrix(*in_cloud_ptr, cluster_idx_vec[k], covariance_matrix, xyz_centroid);
        Eigen::EigenSolver<Eigen::Matrix3f> es(covariance_matrix);
        Eigen::Matrix3f eigenvalues = es.pseudoEigenvalueMatrix();
        Eigen::Matrix3f eigenvectors = es.pseudoEigenvectors();

        std::vector<std::pair<float,Eigen::Vector3f> > tmp_vals(3);
        for (int k = 0; k < 3; ++k) {
            tmp_vals[k].first = eigenvalues(k,k);
            tmp_vals[k].second = Eigen::Vector3f(eigenvectors(0,k),eigenvectors(1,k),eigenvectors(2,k));
        }
        std::sort(tmp_vals.begin(),tmp_vals.end(),
                  [](std::pair<float,Eigen::Vector3f>& pair1,std::pair<float,Eigen::Vector3f>& pair2){
                      return pair1.first < pair2.first;
        });

        float line_val = (tmp_vals[0].first + tmp_vals[1].first) / (tmp_vals[0].first + tmp_vals[1].first + tmp_vals[2].first);
        float line_error_vals = 0.;
        Eigen::Vector3f pt_in_line = Eigen::Vector3f(xyz_centroid(0),xyz_centroid(1),xyz_centroid(2));
        for (int i = 0; i < cluster_idx_vec[k].size(); ++i) {
            const auto& pt = in_cloud_ptr->points[cluster_idx_vec[k][i]];
            line_error_vals += calcuPointToLine(tmp_vals[2].second,pt_in_line,pt);
        }
        line_error_vals /= static_cast<float>(cluster_idx_vec[k].size());

//        std::cout<<"line: "<<line_val<<" "<<line_error_vals<<std::endl;
        if (line_val < config_.line_thre && line_error_vals < config_.line_error_thre){
            line_data_.idx.push_back(cluster_idx_vec[k]);
            line_data_.vec.push_back(tmp_vals[2].second);
            line_data_.center.push_back(pt_in_line);
            continue;
        }

        Eigen::Vector4f plane_parameters;
        plane_parameters(0) = tmp_vals[0].second(0);
        plane_parameters(1) = tmp_vals[0].second(1);
        plane_parameters(2) = tmp_vals[0].second(2);
        plane_parameters(3) = -(plane_parameters(0) * xyz_centroid(0) + plane_parameters(1) * xyz_centroid(1) +
        plane_parameters(2) * xyz_centroid(2));

        float plane_val = tmp_vals[0].first / (tmp_vals[0].first + tmp_vals[1].first + tmp_vals[2].first);
        float plane_error_vals = 0.;
        for (int i = 0; i < cluster_idx_vec[k].size(); ++i) {
            const auto& pt = in_cloud_ptr->points[cluster_idx_vec[k][i]];
            plane_error_vals += calcuPointToPlane(plane_parameters,pt);
        }
        plane_error_vals /= static_cast<float>(cluster_idx_vec[k].size());

//        std::cout<<"plane: "<<plane_val<<" "<<plane_error_vals<<std::endl;

        if (plane_val < config_.plane_thre && plane_error_vals < config_.plane_error_thre){
            plane_data_.idx.push_back(cluster_idx_vec[k]);
            plane_data_.vec.push_back(tmp_vals[0].second);
            plane_data_.center.push_back(pt_in_line);
        }else{
            if (line_error_vals < plane_error_vals){
                line_data_.idx.push_back(cluster_idx_vec[k]);
                line_data_.vec.push_back(tmp_vals[2].second);
                line_data_.center.push_back(pt_in_line);
            }else{
                plane_data_.idx.push_back(cluster_idx_vec[k]);
                plane_data_.vec.push_back(tmp_vals[0].second);
                plane_data_.center.push_back(pt_in_line);
            }
        }
    }

    std::cout<<"all cost: "<<(clock() - start) * 0.001<<" ms."<<std::endl;
//-------------------------test code: test line and plane-----------------------
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr color_cloud_ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
//    for (int k = 0; k < static_cast<int>(line_data_.center.size()); ++k) {
//        for (int i = 0; i < line_data_.idx[k].size(); ++i) {
//            pcl::PointXYZRGB tmp_rgb;
//            const auto& pt = in_cloud_ptr->points[line_data_.idx[k][i]];
//            tmp_rgb.x = pt.x;
//            tmp_rgb.y = pt.y;
//            tmp_rgb.z = pt.z;
//            tmp_rgb.r = 255;
//            color_cloud_ptr->push_back(tmp_rgb);
//        }
//    }
//    for (int k = 0; k < static_cast<int>(plane_data_.center.size()); ++k) {
//        for (int i = 0; i < plane_data_.idx[k].size(); ++i) {
//            pcl::PointXYZRGB tmp_rgb;
//            const auto& pt = in_cloud_ptr->points[plane_data_.idx[k][i]];
//            tmp_rgb.x = pt.x;
//            tmp_rgb.y = pt.y;
//            tmp_rgb.z = pt.z;
//            tmp_rgb.g = 255;
//            color_cloud_ptr->push_back(tmp_rgb);
//        }
//    }
//    pcl::visualization::CloudViewer viewer ("Viewer");
//    viewer.showCloud (color_cloud_ptr);
//    while (!viewer.wasStopped ())
//    {
//    }

//------------------------test code: test clusters---------------------
//    cluster_.clear();
//    for (int k = 0; k < total_obj_num; ++k) {
//        if (cluster_idx_vec[k].size() < config_.cluster_pts_thre){
//            continue;
//        }
//        pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>);
//        tmp_cloud_ptr->resize(cluster_idx_vec[k].size());
//        for (size_t i = 0; i < cluster_idx_vec[k].size(); ++i) {
//            tmp_cloud_ptr->points[i] = in_cloud_ptr->points[cluster_idx_vec[k][i]];
//        }
//        cluster_.push_back(tmp_cloud_ptr);
//    }
//
//    std::cout<<"cluster size: "<<cluster_.size()<<std::endl;
//
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr color_cloud_ptr(new pcl::PointCloud<pcl::PointXYZRGB>);
//    for (int j = 0; j < cluster_.size(); ++j)
//    {
//        cv::Scalar tmp_scalar;
//        tmp_scalar[0] = rand() % 128 + 128;
//        tmp_scalar[1] = rand() % 128 + 128;
//        tmp_scalar[2] = rand() % 128 + 128;
//        for (int i = 0; i < cluster_[j]->size(); ++i)
//        {
//            pcl::PointXYZI tmp_point_i = cluster_[j]->points[i];
//            pcl::PointXYZRGB tmp_point_rgb;
//            tmp_point_rgb.x = tmp_point_i.x;tmp_point_rgb.y = tmp_point_i.y;tmp_point_rgb.z = tmp_point_i.z;
//            tmp_point_rgb.r = tmp_scalar[0];tmp_point_rgb.g = tmp_scalar[1];tmp_point_rgb.b = tmp_scalar[2];
//            color_cloud_ptr->push_back(tmp_point_rgb);
//        }
//    }
//
//    pcl::visualization::CloudViewer viewer ("Viewer");
//    viewer.showCloud (color_cloud_ptr);
//    while (!viewer.wasStopped ())
//    {
//    }
}

void Feature::getLineRes(std::vector<std::vector<int> >& idx,std::vector<Eigen::Vector3f>& dir,std::vector<Eigen::Vector3f>& centers){
    idx = line_data_.idx;
    dir = line_data_.vec;
    centers = line_data_.center;
}
void Feature::getPlaneRes(std::vector<std::vector<int> >& idx,std::vector<Eigen::Vector3f>& norms,std::vector<Eigen::Vector3f>& centers){
    idx = plane_data_.idx;
    norms = plane_data_.vec;
    centers = plane_data_.center;
}

float Feature::calcuPointToLine(const Eigen::Vector3f& dir,const Eigen::Vector3f& pt_in_line,const pcl::PointXYZI& pt){
    Eigen::Vector3f vec;
    vec(0) = pt.x - pt_in_line(0);
    vec(1) = pt.y - pt_in_line(1);
    vec(2) = pt.z - pt_in_line(2);
    float norm1 = dir.norm();
    float norm2 = vec.norm();
    float theta = std::acos(dir.dot(vec) / norm1 / norm2);
    float tmp_dis = std::sqrt((pt_in_line(0) - pt.x) * (pt_in_line(0) - pt.x) +
    (pt_in_line(1) - pt.y) * (pt_in_line(1) - pt.y) + (pt_in_line(2) - pt.z) * (pt_in_line(2) - pt.z));
    return tmp_dis * std::sin(theta);

}
float Feature::calcuPointToPlane(const Eigen::Vector4f& plane_param,const pcl::PointXYZI& pt){
    float a = plane_param(0);
    float b = plane_param(1);
    float c = plane_param(2);
    float d = plane_param(3);
    float tmp_val = sqrtf(a * a + b * b + c * c);
    return std::abs(a * pt.x + b * pt.y + c * pt.z + d) / tmp_val;
}